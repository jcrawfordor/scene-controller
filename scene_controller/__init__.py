from paho.mqtt import client as mqtt_client
import keyboard
import argparse
import logging
import json


def run():
    parser = argparse.ArgumentParser(prog="Scene Controller", description="MQTT Scene Controller")
    parser.add_argument("host", type=str)
    parser.add_argument("-p", "--port", help="Port number", default=1883, type=int)
    parser.add_argument("-t", "--topic", help="Topic to publish on", default="scenecontroller", type=str)
    parser.add_argument("-u", "--user", help="MQTT broker username", type=str)
    parser.add_argument("-w", "--password", help="MQTT broker password", type=str)
    parser.add_argument("-c", "--client", help="Client ID string", type=str, default="scenecontroller")
    parser.add_argument('-d', '--debug', help="Debug output", action="store_const", dest="loglevel",
                        const=logging.DEBUG, default=logging.WARNING)
    parser.add_argument('-v', '--verbose', help="Verbose output", action="store_const", dest="loglevel",
                        const=logging.INFO)
    args = parser.parse_args()

    logging.basicConfig(level=args.loglevel)
    logging.debug("Debug logging enabled")

    # MQTT connection
    client = mqtt_client.Client(args.client)
    if args.user:
        client.username_pw_set(args.user, args.password)
    client.connect(args.host, args.port)
    logging.info("Connected to MQTT broker")

    def handle_key(event):
        logging.debug(f"Keypad: {event.name} ({event.scan_code})")
        payload = {
            'device': args.client,
            'key': event.name
        }
        while True:
            try:
                client.connect(args.host, args.port)
                res = client.publish(args.topic, json.dumps(payload))
                res.wait_for_publish()
                logging.debug(f"Published to {args.topic}: {json.dumps(payload)}, got {res.rc}")
                break
            except RuntimeError:
                logging.exception("Failed publishing event, will try again.")

    logging.info("Monitoring keypad...")
    keyboard.on_press(handle_key)
    keyboard.wait()


if __name__ == "__main__":
    run()

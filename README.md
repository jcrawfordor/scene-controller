# Scene Controller

This simple Python daemon makes it easy to use a programmable keypad (or
keyboard or whatever) as a scene controller for home automation. I wrote
this to be used in a pretty specific use case, but it can also be used
for much more general purposes.

## Use with Home Assistant

Start out with some kind of Linux single-board computer and a keypad or
keyboard you'd like as a scene controller. I use Genovation CP24
programmable control pads, which can regularly be found on eBay for as
little as $25. Whether you use a programmable keypad or something more
basic like a USB ten-key is up to you, as this service doesn't make any
particular assumptions about the scancodes.

That said, purely for
convenience, I use Genovation MacroMaster CPxx to program the keypads
to emit scan codes for letter keys A-X, using 24 letters of the alphabet
to cover the 24 keys. I just find this easy to remember when mapping keys
later. Note that if you use 2x1 or even 2x2 or larger keys, the CP24 will
emit multiple events for each press due to the multiple keyswitches under the keycap. You could
just ignore all of the keys except for one, or you could use MacroMaster
to program the keypad so that only one of the switches under the larger
keycap emits a keypress.

The SBC should be running some kind of Linux with Python 3, that should
be all that's required.

### Prerequisites

This service will send events via MQTT. An easy way to get an MQTT broker
set up is to use the Mosquito MQTT broker add-on for HomeAssistant. This
add-on uses HomeAssistant users for authentication out of the box, so
you can create a HomeAssistant user to be a "service user" for
scene-controller to authenticate.

### Installation

1. `pip install git+https://gitlab.com/jcrawfordor/scene-controller.git`
   (be advised you will need a reasonably up-to-date version of pip for
   this cool install-from-pyproject.toml-in-a-git-repo trick to work)
2. Copy `scene-controller.service` to `/etc/systemd/system`
3. Edit `/etc/systemd/system/scene-controller.service` to include the correct
   MQTT connection info. Host, username, password are required. Client ID
   (`-c`) is optional, if you have more than one device you should set it
   to a unique value on each so that HomeAssistant can tell them apart.
4. `systemctl daemon-reload`, `systemctl enable --now scene-controller`

### Usage

Whenever a key is pressed, scene-controller will publish a message to
the MQTT topic specified by `-t`, or `scenecontroller` by default. The
message will look like `{"device": "ls2", "key": "b"}`, "device" being
the client ID specified by `-c` and "key" the display name of the scan
code of the key pressed. This is usually a letter or a number but
special keys come across with mostly sane values like `ESC`.

Included is a Home Assistant blueprint which simplifies the HA end. It
assumes the keypad will emit letters A up to P, although you could
add more potential actions with some tedious copy-paste action. You can
configure the keypad client ID and action for each keypress. Unfortunately
HomeAssistant won't let you import blueprints from GitLab because they're
Microsoft loyalists or something, but you can put the blueprint file
directly in the blueprints directory in the HomeAssistant configuration.
